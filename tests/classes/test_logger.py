import unittest

import os.path
import datetime
import time


from classes.logger import *

 
class TestLoggerFormatsCase(unittest.TestCase):
    def setUp(self):
        self.format_unix = UnixFormatStrategy()
        self.format_human = HumanFormatStrategy()
        self.msg = 'message'

    def test_write_to_log_unix(self):
        s = self.format_unix.get_format_str(self.msg)
        self.assertEqual(s, str(int(time.time())) + ' :: ' + self.msg + '\n')

    def test_write_to_log_human(self):
        s = self.format_human.get_format_str(self.msg)
        now = datetime.datetime.now()
        self.assertEqual(s, now.strftime("%Y-%m-%d %H:%M") + ' :: ' + self.msg + '\n' )        


if __name__ == '__main__':
    unittest.main()        
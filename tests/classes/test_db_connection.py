import unittest

import psycopg2

from classes.logger import *
import credentials
import classes.exceptions
from classes.db_connection import DB_connection

 
class TestDB_conectionCase(unittest.TestCase):
    def setUp(self):
        logger = Logger(HumanFormatStrategy)
        self.db_connection = DB_connection(credentials, classes.exceptions, logger)   

    def test_db_connection(self):
        enter = self.db_connection.__enter__()
        self.assertNotEqual(enter, None)


if __name__ == '__main__':
    unittest.main()        
#!/usr/bin/env python3
# encoding: utf-8

import psycopg2

from classes.logger import *
import credentials
import classes.exceptions
from classes.db_connection import DB_connection





db_connection = DB_connection(credentials, classes.exceptions, Logger(HumanFormatStrategy))

with db_connection as db_conn:
    db_conn.set_client_encoding('UTF8')
    db_cursor = db_conn.cursor() 
    db_cursor.execute('SELECT * FROM parse_results2');   
    content = db_cursor.fetchall() 

    print("Content-type: text/html")
    print()
    print("<h1>Joba finder</h1>")    

    with open('journal.txt', 'r', encoding='utf-8') as f:
        last_line = f.readlines()[-1]
        print('<h3>last parse: ' + last_line.split('::')[0] + '</h3>\n\n')    
        print('<h3>last save: ' + last_line.split('::')[1] + '</h3>\n\n')    

    print('<!DOCTYPE HTML>\
    <html>\
    <head>\
    <meta charset="utf-8">\
    <title>Обработка данных форм</title>\
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">\
    </head>\
    <body>\
    <table border=1 cellspacing=0 cellpadding=4>')

    for row in content:
        print('<tr title="' + row[5] + '">' + 
        '<td>'+row[1]+'</td>' + '<td>'+row[2]+'</td>' + '<td>'+row[3]+'</td>' + '<td>'+row[4]+'</td>' +
        '</tr>')

    print('</table><br>')
    print('<a href="/results.txt" download>Скачать файл</a>')
    print('</body></html>')




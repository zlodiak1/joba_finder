import config
import credentials
import classes.exceptions

from classes.parser import Parser
from classes.db_connection import DB_connection
from classes.sql_requester import Sql_requester
from classes.journal import Journal
from classes.logger import *
from classes.main import Main
from classes.saver import *


if __name__ == '__main__': 
    logger = Logger(HumanFormatStrategy)
    db_connection = DB_connection(credentials, classes.exceptions, logger)
    parser = Parser(config, classes.exceptions, logger)
    save_strategy = DBSaveStrategy()
    journal = Journal(config.journal_file_name, save_strategy)

    main = Main(classes.exceptions)
    main.set_db_connection(db_connection)
    main.set_parser(parser)
    main.set_logger(logger)
    main.set_saver(Saver)
    main.set_save_strategy(save_strategy)
    main.set_Sql_requester(Sql_requester)
    main.set_journal(journal)
    main.set_results_file_name(config.results_file_name)

    main.run()

    


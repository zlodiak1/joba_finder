import os.path
import datetime
import time

from abc import ABCMeta, abstractmethod


class Logger:
    def __init__(self, Format_strategy):
        self.format_strategy = Format_strategy()

    def write_to_log(self, msg):
        with open('error_log.txt', 'a', encoding='utf-8') as f:
            f.write(self.format_strategy.get_format_str(str(msg)))


class IFormatStrategy(metaclass=ABCMeta):
    @abstractmethod
    def get_format_str(self, msg):
        pass


class UnixFormatStrategy(IFormatStrategy):
    def get_format_str(self, msg):
        return str(int(time.time())) + ' :: ' + msg + '\n'


class HumanFormatStrategy(IFormatStrategy):
    def get_format_str(self, msg):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d %H:%M") + ' :: ' + msg + '\n'        

import datetime

class Journal:
    def __init__(self, journal_file_name, save_strategy):
        self.journal_file_name = journal_file_name
        self.save_strategy = save_strategy

    def write(self):
        now = datetime.datetime.now()
        with open('journal.txt', 'a', encoding='utf-8') as f:
            f.write(now.strftime("%Y-%m-%d %H:%M") + ' :: ' + str(self.save_strategy) + '\n'  )        
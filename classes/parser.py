import requests
from bs4 import BeautifulSoup

class Parser:
    def __init__(self, config, exceptions, logger):
        self.config = config
        self.exceptions = exceptions
        self.logger = logger

    def get_content(self):
        try:
            root_html = self.get_html(self.config.url)
            links = self.get_links(root_html)
            content = self.get_content_inner(links)
            return content
        except (self.exceptions.ErrorGetHtml, 
                self.exceptions.ErrorGetLinks, 
                self.exceptions.ErrorFormatData, 
                self.exceptions.ErrorParseInnerHtml) as e:
            msg = 'geting content for parse is failed. ' + str(e)
            self.logger.write_to_log(msg)
            raise self.exceptions.ErrorGetContent(msg)

    def get_html(self, url):
        try:
            html = requests.get(url, headers=self.config.headers).text
            return html
        except Exception as e:
            msg = 'getting html of ' + url + ' is failed. ' + str(e)
            self.logger.write_to_log(msg)
            raise self.exceptions.ErrorGetHtml(msg)

    def get_links(self, html):
        links = []

        try:
            soup = BeautifulSoup(html, 'lxml')
            links_obj = soup.find('div', 'vacancy-serp').findAll('a', {'class': 'HH-LinkModifier'})
        except Exception as e:
            msg = 'getting links to inner pages is failed. ' + str(e)
            self.logger.write_to_log(msg)
            raise self.exceptions.ErrorGetLinks(msg)

        for link in links_obj:
            links.append(link.get('href'))

        return links

    def get_content_inner(self, links):
        content = []
        for link in links:
            html = self.get_html(link)

            try:
                soup = BeautifulSoup(html, 'lxml')
                desc =          soup.find('div', {'class': 'vacancy-description'})
                pay =           soup.find('p', {'class': 'vacancy-salary'})
                company =       soup.find('span', {'itemprop': 'name'})
                position =      soup.find('h1', {'itemprop': 'title'})
                public_date =   soup.find('p', {'class': 'vacancy-creation-time'})
            except Exception as e:
                msg = 'parse of inner page is failed. ' + str(e)
                self.logger.write_to_log(msg)
                raise self.exceptions.ErrorParseInnerHtml(msg)

            try:
                content_inner = {
                    'desc':         str(desc.text).strip(),
                    'pay':          pay.text.strip(),
                    'company':      company.text.strip(),
                    'position':     position.text.strip(),
                    'public_date':  public_date.text.strip(),
                }
            except Exception as e:
                msg = 'formating data of inner page is failed. ' + str(e)
                self.logger.write_to_log(msg)
                raise self.exceptions.ErrorFormatData(msg)

            content.append(content_inner)

        return content
                      
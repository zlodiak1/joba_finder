class Main:
    def __init__(self, exceptions):
        self.exceptions = exceptions
        self.content = []

    def set_db_connection(self, db_connection):
        self.db_connection = db_connection

    def set_parser(self, parser):
        self.parser = parser

    def set_logger(self, logger):
        self.logger = logger

    def set_Sql_requester(self, Sql_requester):
        self.Sql_requester = Sql_requester

    def set_saver(self, Saver):
        self.Saver = Saver   

    def set_save_strategy(self, save_strategy):
        self.save_strategy = save_strategy    

    def set_results_file_name(self, results_file_name):
        self.results_file_name = results_file_name

    def set_journal(self, journal):
        self.journal = journal

    def run(self):
        try:
            self.get_content()
            self.save_content()
            self.journal.write()
        except (self.exceptions.ErrorGetContent, 
                self.exceptions.ErrorConnectDB, 
                self.exceptions.ErrorClearTable, 
                self.exceptions.ErrorSaveContent) as e:
            msg = e
            self.logger.write_to_log(msg)
            print('E::', msg)   # debug info        

    def get_content(self):
        self.content = self.parser.get_content()           

    def save_content(self):
        saver = self.Saver(self.db_connection, self.Sql_requester, self.exceptions, self.logger, self.save_strategy, self.results_file_name)
        saver.save(self.content)

     



class Sql_requester:
    def __init__(self, db_cursor, exceptions):
        self.db_cursor = db_cursor
        self.exceptions = exceptions

    def clear_table(self):
        try:
            self.db_cursor.execute('DELETE FROM parse_results2');      
        except Exception as e:
            msg = 'Error clear table. ' + str(e)
            self.logger.write_to_log(msg)
            raise self.exceptions.ErrorClearTable(msg)

    def fill_table(self, content):
        try:
            for c in content:
                req = 'INSERT INTO parse_results2 (public_date, position, company, pay, description) VALUES (%s,%s,%s,%s,%s)'
                vals = (
                    c['public_date'],
                    c['position'],
                    c['company'],
                    c['pay'],
                    c['desc']
                )
                self.db_cursor.execute(req, vals);    
        except Exception as e:
            msg = 'Table inserting is failed. ' + str(e)
            self.logger.write_to_log(msg)
            raise self.exceptions.ErrorTableInsert(msg)  

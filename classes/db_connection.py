import psycopg2

class DB_connection:
    def __init__(self, credentials, exceptions, logger):
        self.credentials = credentials
        self.exceptions = exceptions
        self.logger = logger
    
    def __enter__(self):
        try:
            self.connection = psycopg2.connect(
                dbname=self.credentials.dbname, 
                user=self.credentials.user, 
                password=self.credentials.password, 
                host=self.credentials.host
            )
            self.connection.autocommit = True
            return self.connection
        except Exception as e:
            msg = 'DB connection is failed. ' + str(e)
            self.logger.write_to_log(msg)
            raise self.exceptions.ErrorConnectDB(msg)      
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        if hasattr(self, 'connection'):
            self.connection.close()   
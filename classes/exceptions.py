class ErrorGetHtml(Exception):
    def __init__(self, msg):
        pass

class ErrorGetLinks(Exception):
    def __init__(self, msg):
        pass       

class ErrorFormatData(Exception):
    def __init__(self, msg):
        pass

class ErrorParseInnerHtml(Exception):
    def __init__(self, msg):
        pass

class ErrorGetContent(Exception):
    def __init__(self, msg):
        pass 

class ErrorConnectDB(Exception):
    def __init__(self, msg):
        pass        

class ErrorClearTable(Exception):
    def __init__(self, msg):
        pass

class ErrorSaveContent(Exception):
    def __init__(self, msg):
        pass          

class ErrorTableInsert(Exception):
    def __init__(self, msg):
        pass         
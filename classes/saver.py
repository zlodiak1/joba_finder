from abc import ABCMeta, abstractmethod

class Saver:
    def __init__(self, db_connection, Sql_requester, exceptions, logger, save_strategy, results_file_name):
        self.db_connection = db_connection
        self.Sql_requester = Sql_requester
        self.exceptions = exceptions
        self.logger = logger
        self.save_strategy = save_strategy
        self.results_file_name = results_file_name

    def save(self, content):
        self.save_strategy.save(self.db_connection, self.Sql_requester, self.exceptions, self.logger, content, self.results_file_name)


class ISaveStrategy(metaclass=ABCMeta):
    @abstractmethod
    def save(self):
        pass


class DBSaveStrategy(ISaveStrategy):
    def save(self, db_connection, Sql_requester, exceptions, logger, content, results_file_name):
        with db_connection as db_connection:
            db_cursor = db_connection.cursor() 
            try:
                sql_requester = Sql_requester(db_cursor, exceptions)
                sql_requester.clear_table()
                sql_requester.fill_table(content)
            except (exceptions.ErrorClearTable,
                    exceptions.ErrorTableInsert) as e:
                msg = 'Save DB content is failed. ' + str(e)
                logger.write_to_log(msg)
                raise exceptions.ErrorSaveContent(msg)

    def __str__(self):
        return 'Save into DB'                


class FileSaveStrategy(ISaveStrategy):
    def save(self, db_connection, Sql_requester, exceptions, logger, content, results_file_name):                
        with open(results_file_name, 'w', encoding='utf-8') as f:
            for s in content:  
                for key, val in s.items():              
                    f.write(str(key).upper() + ': ' + str(val) + '\n')   
                f.write('\n\n')   

    def __str__(self):
        return 'Save into file'                   